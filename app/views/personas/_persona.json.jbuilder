json.extract! persona, :id, :name, :height, :mass, :hair_color, :created_at, :updated_at
json.url persona_url(persona, format: :json)
