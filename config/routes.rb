Rails.application.routes.draw do
	get 'ver_mas/:url', :to => 'personas#ver_mas', :as => 'ver_mas'
  resources :personas
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
