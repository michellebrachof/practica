class CreatePersonas < ActiveRecord::Migration[5.0]
  def change
    create_table :personas do |t|
      t.string :name
      t.string :height
      t.integer :mass
      t.string :hair_color

      t.timestamps
    end
  end
end
